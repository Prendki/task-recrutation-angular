# Task for recrutation

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Requirements

- Node + NPM
- Browser
- Angular CLI (ng commands)

## Quick start

- Run `npm install`
- Run `ng serve`
- Launch browser with `http://localhost:4200`

That's all :)

*...If you have problems starting the project, please let me know*
