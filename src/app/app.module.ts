import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {MaterialModule} from './material/material.module';
import { SearchComponent } from './home/search/search.component';
import { ReplaceComponent } from './home/replace/replace.component';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {FormControl, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ErrorStateMatcher} from "@angular/material";
import { ArticlesComponent } from './home/articles/articles.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    ReplaceComponent,
    ArticlesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    // Material Assets
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ErrorStateMatcher
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
