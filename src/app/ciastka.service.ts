import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CiastkaService {
  constructor() {
  }

  getCiastki() {
    const ciastki = Math.floor(Math.random() * 20);
    // Ten const jest kompletnie nie potrzebny, ale ładnie wygląda
    const randomBoolean = Math.random() >= 0.5;
    const almostBooleanBecauseWeWriteInJs = Math.round(Math.random())

    const tablicazmoimiciastkamibezcamelcase = [
      `Masz ${ciastki} ciastków`,
      `Zjadłeś Pan wszystkie ciastki`
    ];

    return tablicazmoimiciastkamibezcamelcase[almostBooleanBecauseWeWriteInJs];
  }
}
