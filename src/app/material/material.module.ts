import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCardFooter,
  MatCardModule,
  MatDatepickerModule, MatDividerModule,
  MatInputModule,
  MatNativeDateModule
} from "@angular/material";
import {FormsModule} from "@angular/forms";



@NgModule({
  exports: [
    MatNativeDateModule,
    MatDatepickerModule,
    MatInputModule,
    MatCardModule,
    MatDividerModule,
    MatButtonModule,
    FormsModule
  ]
})
export class MaterialModule { }
