import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private httpService: HttpClient) {
  }

  public getResults(phrase: string) {
    const options = {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      }
    };
    return this.httpService.get(
      `https://en.wikipedia.org/w/api.php?action=query&list=search&format=json&srsearch=${phrase}&srlimit=10&origin=*`,
      options
    ).pipe(
      map(data => data),
      catchError(() => throwError('Problem :]'))
    );
  }
}
