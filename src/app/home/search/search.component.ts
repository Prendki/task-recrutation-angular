import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SearchService} from './search.service';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  form: FormGroup;
  timeout: any;

  @Output() change = new EventEmitter();

  constructor(private searchService: SearchService, private fb: FormBuilder) {

  }

  ngOnInit(): void {
    this.form = this.fb.group({
      searchField: ['', [Validators.required]]
    });
  }

  search() {
    clearTimeout(this.timeout);
    const value = this.form.value.searchField;
    this.searchService.getResults(value).subscribe((data) => {
      this.change.emit(Object.assign({phrase: value}, data));
    });
  }

  onSearchChange() {
    clearTimeout(this.timeout);
    if (this.form.valid) {
      this.timeout = setTimeout(() => {
        this.search();
      }, 300);
    }
  }
}
