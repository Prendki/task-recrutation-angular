import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-replace',
  templateUrl: './replace.component.html',
  styleUrls: ['./replace.component.scss']
})
export class ReplaceComponent implements OnInit {
  @Input() public articles: any;
  replacePhrase = '';

  constructor() {
  }

  ngOnInit() {
  }

  replace(el = document.querySelector('.searchmatch')) {
    const newEl = document.createElement('span');
    newEl.innerHTML = this.replacePhrase;

    // replace el with newEL
    el.parentNode.replaceChild(newEl, el);
  }

  replaceAll() {
    const elements = document.querySelectorAll('.searchmatch');
    elements.forEach((element) => {
      this.replace(element);
    });
  }

}
