import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  articles: any;
  phrase: string;

  constructor() {

  }

  ngOnInit() {

  }

  change(data) {
    if (data.query) {
      this.articles = data.query.search;
      this.phrase = data.phrase;
    }
  }
}
