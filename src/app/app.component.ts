import {Component} from '@angular/core';
import {CiastkaService} from "./ciastka.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  footerText = null;

  constructor(private ciastkaService: CiastkaService) {
    this.footerText = this.ciastkaService.getCiastki();
  }
}
